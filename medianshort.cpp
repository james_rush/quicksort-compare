#include <iostream>
#include <vector>
#include <algorithm>
#include <ctime>
#include <fstream>
#include <utility>  
#include <climits>

using namespace std;

int  pivotchoose(vector <int> &List,int left,int right){
	vector < pair<int,int> > candidate;
	candidate.push_back(make_pair(List.at(left) ,left));
	candidate.push_back(make_pair(List.at((left + right)/2) ,(left + right)/2));
	candidate.push_back(make_pair(List.at(right) ,right));
	sort(candidate.begin(),candidate.end());
	return candidate.at(1).second;
}

void qsort(vector <int> &List,int left,int right){
    int Pivot,PivotPos,i,j;
    if(left < right){
        i = left;
        j = right + 1;
		PivotPos = pivotchoose(List,left,right);
		iter_swap(List.begin() + left, List.begin() + PivotPos);
        Pivot = List.at(left);
        do{
            do{
                ++i;
            }while(List.at(i) < Pivot);

            do{
                --j;
            }while(List.at(j) > Pivot);

            if(i < j){
                iter_swap(List.begin() + i, List.begin() + j);
            }

        }while( i < j);

        iter_swap(List.begin() + left, List.begin() + j);
		if(j - left < right - j){//short list first
       		qsort(List,left,j - 1);
        	qsort(List,j + 1,right);
		}
		else{
			qsort(List,j + 1,right);
			qsort(List,left,j - 1);
		}

    }
}


int main(){
    int Data;
    clock_t Start,End;
    double Cpu;
    vector <int> List;
	fstream file;
	file.open("test.txt",std::fstream::in);

    while(!file.eof()){
		file >> Data;
        List.push_back(Data);
    }
	List.back() = INT_MAX;
	file.close();

    Start = clock();
    qsort(List,0,List.size() - 2);
    End = clock();
	List.pop_back();	
	/*		
    for(int i = 0 ;i < List.size(); ++i){
        cout << List.at(i) << " ";
    }
	cout << endl;
	*/
    Cpu =((double) (End - Start)) / CLOCKS_PER_SEC;
    cout << "use median and shorter first execution time = " << Cpu << endl;
	
    return 0;
}

