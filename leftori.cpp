#include <iostream>
#include <vector>
#include <algorithm>
#include <ctime>
#include <fstream>
#include <climits>

using namespace std;

void qsort(vector <int> &List,int left,int right){
    int Pivot,i,j;
    if(left < right){
        i = left;
        j = right + 1;
        Pivot = List.at(left);
        do{
            do{
                ++i;
            }while(List.at(i) < Pivot);

            do{
                --j;
            }while(List.at(j) > Pivot);

            if(i < j){
                iter_swap(List.begin() + i, List.begin() + j);
            }

        }while( i < j);

        iter_swap(List.begin() + left, List.begin() + j);
		//original
       	qsort(List,left,j - 1);
        qsort(List,j + 1,right);
    }
}


int main(){
    int Data;
    clock_t Start,End;
    double Cpu;
    vector <int> List;
	fstream file;
	file.open("test.txt",std::fstream::in);

    while(!file.eof()){
		file >> Data;
        List.push_back(Data);
    }
	List.back() = INT_MAX;
	file.close();

    Start = clock();
    qsort(List,0,List.size() - 2);
    End = clock();
	List.pop_back();
	/*	
    for(int i = 0 ;i < List.size(); ++i){
        cout << List.at(i) << " ";
    }
	*/
    Cpu =((double) (End - Start)) / CLOCKS_PER_SEC;
    cout << "use leftmost and original sequence execution time = " << Cpu << endl;

    return 0;
}

